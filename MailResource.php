<?php

namespace Drupal\mail_api\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides a Mail Resource.
 * Take data from a remote contact form and send email.
 *
 * @RestResource(
 *   id = "mail_resource",
 *   label = @Translation("Mail Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/mail_api/send",
 *     "create" = "/api/mail_api/send"
 *   }
 * )
 */
class MailResource extends ResourceBase {

  /**
   * Responds to POST requests.
   *
   * @param mixed $data
   *   Data to send email.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($data) {
    $response = $this->t('Error sending email. @params', ['@params' => print_r($data, TRUE)]);

    // Cast $data to array for consistency.
    if (!is_array($data)) {
      $data = (array) $data;
    }

    // If email data is available.
    if (!empty($data['email']) && !empty($data['subject']) && !empty($data['message'])) {
      // Get mail service.
      $mailManager = \Drupal::service('plugin.manager.mail');
      $module = 'mail_api';
      $key = 'contact_form';

      // Set email data.
      $to = \Drupal::config('system.site')->get('mail');
      $params['from'] = $data['email'];
      $params['message'] = $data['message'];
      $params['subject'] = $data['subject'];

      // Send email.
      $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, TRUE);

      // Configure response.
      if ($result['result'] === TRUE) {
        $response = $this->t('Email sent. @params', ['@params' => print_r($params, TRUE)]);
      }
      else {
        $response = $this->t('Error sending email! @params', ['@params' => print_r($data, TRUE)]);
      }
    }
    else {
      $response = $this->t('Missing parameters. Must have subject, message, and email. @params', ['@params' => print_r($data, TRUE)]);
    }

    // Post response to log messages.
    \Drupal::logger('mail_api')->notice($response);

    // Send response to source.
    return new ResourceResponse($response, 200);
  }

}
