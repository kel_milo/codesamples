<?php

namespace Drupal\thecommon\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Custom configuuration form.
 */
class CommonSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commonsettings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config('thecommon.settings');

    // List of main navigation links.
    $main_nav_list = [
      'home',
      'story',
      'menu',
      'event',
      'booking',
      'location',
    ];

    // Container for main navigation settings.
    $form['main_nav_container'] = [
      '#type' => 'details',
      '#title' => $this->t('Main Navigation'),
      '#prefix' => '<div id="main-navigation-wrapper">',
      '#suffix' => '</div>',
      '#open' => FALSE,
    ];

    foreach ($main_nav_list as $main_nav) {
      // Config name.
      $config_name = $main_nav . '_nav_label';

      // Title to be used as default.
      $title = ucfirst($main_nav);

      // Create form element.
      $default = $config->get($config_name);
      $form['main_nav_container'][$config_name] = [
        '#type' => 'textfield',
        '#title' => $this->t('"@title" label', ['@title' => $title]),
        '#default_value' => !empty($default) ? $default : $title,
        '#description' => $this->t('Label for "@title" in main navigation menu.', ['@title' => $title]),
      ];
    }

    // Container for Story settings.
    $form['story_container'] = [
      '#type' => 'details',
      '#title' => $this->t('Story'),
      '#prefix' => '<div id="story-wrapper">',
      '#suffix' => '</div>',
      '#open' => FALSE,
    ];

    $default = $config->get('story_text');
    $form['story_container']['story_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Our Story'),
      '#default_value' => !empty($default) ? $default : '',
      '#description' => $this->t('Content for "Story page.'),
    ];

    // Container for Location settings.
    $form['location_container'] = [
      '#type' => 'details',
      '#title' => $this->t('Location'),
      '#prefix' => '<div id="location-wrapper">',
      '#suffix' => '</div>',
      '#open' => FALSE,
    ];

    $default = $config->get('location_text');
    $form['location_container']['location_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Location description'),
      '#default_value' => !empty($default) ? $default : '',
      '#description' => $this->t('Content for "Location page.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get an array of form values.
    $values = $form_state->getValues();
    $config = \Drupal::configFactory()->getEditable('thecommon.settings');

    $ignore_keys = [
      '_core',
      'submit',
      'form_build_id',
      'form_token',
      'form_id',
      'op',
    ];

    // Loop through the values and save them to configuration.
    foreach ($values as $value_key => $value) {
      if (in_array($value_key, $ignore_keys)) {
        continue;
      }
      $config->set($value_key, $value)->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'thecommon.settings',
    ];
  }

}
