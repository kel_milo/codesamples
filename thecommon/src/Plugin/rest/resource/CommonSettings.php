<?php

namespace Drupal\thecommon\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Annotation for get method.
 *
 * @RestResource(
 *   id = "thecommon_settings_get",
 *   label = @Translation("The Common Settings GET"),
 *   uri_paths = {
 *     "canonical" = "/the-common-settings"
 *   }
 * )
 */
class CommonSettings extends ResourceBase {

  /**
   * Responds to GET requests.
   *
   * Returns a list of site settings.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get() {
    $common_config = \Drupal::config('thecommon.settings');
    $common_values = $common_config->get();

    // Create the rest response.
    $response = [];
    foreach ($common_values as $key => $value) {
      // For preg-match.
      $matches = [];

      // Ignore _core in response.
      if ($key == '_core') {
        continue;
      }
      // If settings is for menu labels, put into separate array.
      elseif (preg_match('/(.*)_nav_label/', $key, $matches)) {
        $response['navLabels'][$matches[1]] = $value;
      }
      // Default to just set the value to the key.
      else {
        $response[$key] = $value;
      }
    }

    return new ResourceResponse($response);
  }

}
