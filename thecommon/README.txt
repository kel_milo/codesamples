The Common
=======

This module provides a user interface for managing The Common co-working
space web site. It's a headless Drupal site which shares data via REST API.
