<?php

namespace Drupal\rest_api_alter\Normalizer;

use Drupal\serialization\Normalizer\ContentEntityNormalizer;
use Drupal\paragraphs\Entity\Paragraph;

 /**
 * EntityReferenceNormalizer.
 *
 * Check if node has specific Paragraph field, if so load those
 * Paragraph entities and add to normalized array.
 *
 */
class EntityReferenceNormalizer extends ContentEntityNormalizer {
  /**
   * The interface or class that this Normalizer supports.
   *
   * @var string
   */
  protected $supportedInterfaceOrClass = 'Drupal\node\NodeInterface';

  /**
   * {@inheritdoc}
   */
  public function normalize($entity, $format = NULL, array $context = []) {
    // Normalize entity.
    $attributes = parent::normalize($entity, $format, $context);

    // Get current language code.
    $lang = $entity->language()->getId();

    // Loop through field_label values if field exists.
    if (!empty($attributes['field_labels'])) {
      $field_labels = [];
      foreach ($attributes['field_labels'] as &$label) {
        if (empty($label['target_id'])) {
          continue;
        }
        // Load paragraph entity.
        $paragraph = Paragraph::load($label['target_id']);

        if (!empty($paragraph)) {
          // Get translated paragraph if necessary.
          if (!empty($lang)) {
            $translated_paragraph = $paragraph->getTranslation($lang);
            $paragraph = (!empty($translated_paragraph)) ? $translated_paragraph : $paragraph;
          }
          // Normalize paragraph for output.
          $normalized_paragraph = parent::normalize($paragraph, $format, $context);

          // Add label to output.
          if (!empty($normalized_paragraph['field_label'][0]['value'])) {
            $label = $normalized_paragraph['field_label'][0]['value'];
          }

          // Add machine name to output.
          if (!empty($normalized_paragraph['field_machine_name'][0]['value'])) {
            $machine_name = $normalized_paragraph['field_machine_name'][0]['value'];
          }

          if (!empty($label) && !empty($machine_name)) {
            $field_labels[$machine_name] = $label;
          }
        }
      }

      $attributes['field_labels'] = $field_labels;
    }

    // Return the $attributes with our new values.
    return $attributes;
  }

}
